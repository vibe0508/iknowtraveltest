//
//  Content.h
//  TestTask
//
//  Created by Вячеслав Бельтюков on 17.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Mappable.h"

NS_ASSUME_NONNULL_BEGIN

@interface Content : NSManagedObject<Mappable>

@property (nonatomic) ContentType type;

@end

NS_ASSUME_NONNULL_END

#import "Content+CoreDataProperties.h"

//
//  ContentCellModel.h
//  TestTask
//
//  Created by Вячеслав Бельтюков on 17.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Content.h"

@interface ContentCellModel : NSObject

@property (nonatomic) Content *content;

@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSString *subtitle;

- (void)getImageDataToBlock:(DataBlock)block;

+ (instancetype)modelWithContent:(Content*)content;

@end

//
//  ContentViewModel.h
//  TestTask
//
//  Created by Вячеслав Бельтюков on 17.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Content;

@interface ContentViewModel : NSObject

@property (nonatomic, readonly) NSString *creatorName;
@property (nonatomic, copy) VoidBlock updateHandler;

+ (instancetype) viewModelForContent:(Content*)content;

@end

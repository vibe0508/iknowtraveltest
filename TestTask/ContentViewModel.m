//
//  ContentViewModel.m
//  TestTask
//
//  Created by Вячеслав Бельтюков on 17.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "ContentViewModel.h"
#import "ContentManager.h"
#import "Creator.h"

@interface ContentViewModel ()

@property (nonatomic, readonly) Content *content;

@end

@implementation ContentViewModel

+ (instancetype)viewModelForContent:(Content *)content{
    ContentViewModel *viewModel = [[self alloc] initWithContent:content];
    return viewModel;
}

- (instancetype)initWithContent:(Content*)content{
    if (self = [super init]) {
        _content = content;
        DeclareWeakSelf
        [[ContentManager sharedInstance] fetchInfoToContent:content completion:^{
            if (weakSelf.updateHandler) {
                weakSelf.updateHandler();
            }
        }];
    }
    return self;
}

- (NSString *)creatorName{
    return self.content.creator.name;
}

@end

//
//  ContentTableViewCell.m
//  TestTask
//
//  Created by Вячеслав Бельтюков on 17.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "ContentTableViewCell.h"

@interface ContentTableViewCell ()

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *subtitleLabel;
@property (nonatomic, weak) UIImageView *backgroundImageView;

@end

@implementation ContentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    UIImageView *imageView = [UIImageView new];
    self.backgroundView = imageView;
    self.backgroundImageView = imageView;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(ContentCellModel *)model{
    _model = model;
    [self updateViews];
}

- (void)updateViews{
    self.titleLabel.text = self.model.title;
    self.subtitleLabel.text = self.model.subtitle;
    self.backgroundImageView.image = nil;
    DeclareWeakSelf
    NSString *title= self.model.title;
    [self.model getImageDataToBlock:^(NSData *data) {
        if ([weakSelf.model.title isEqualToString:title]) {
            weakSelf.backgroundImageView.image = [UIImage imageWithData:data];
        }
    }];
}

@end

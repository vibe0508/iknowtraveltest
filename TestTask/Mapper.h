//
//  Mapper.h
//  TestTask
//
//  Created by Вячеслав Бельтюков on 20.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "Singleton.h"

@interface Mapper : Singleton

- (void)registerClass:(Class)aClass;
- (NSArray*)mapFromDictionary:(NSDictionary*)dict;
- (NSArray*)mapObjectsFromArray:(NSArray*)array withClass:(Class)aClass;
- (id)mapObject:(NSDictionary*)dict withClass:(Class)aClass;

@end

//
//  Creator.h
//  TestTask
//
//  Created by Вячеслав Бельтюков on 20.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Mappable.h"

NS_ASSUME_NONNULL_BEGIN

@interface Creator : NSManagedObject<Mappable>

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Creator+CoreDataProperties.h"

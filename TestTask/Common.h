//
//  Common.h
//  TestTask
//
//  Created by Вячеслав Бельтюков on 17.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#ifndef Common_h
#define Common_h

#import <Foundation/Foundation.h>

typedef void(^ObjectBlock)(id obj);
typedef void(^ArrayBlock)(NSArray *array);
typedef void(^DataBlock)(NSData *data);
typedef void(^ErrorBlock)(NSError *error);
typedef void(^VoidBlock)();

static double kCellHeight = 100.0;

#define MainContext ((AppDelegate*)[[UIApplication sharedApplication] delegate]).managedObjectContext
#define SaveMainContext [((AppDelegate*)[[UIApplication sharedApplication] delegate]) saveContext];
#define DeclareWeakSelf __weak typeof(self) weakSelf = self;

#endif /* Common_h */

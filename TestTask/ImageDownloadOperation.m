//
//  ImageDownloadOperation.m
//  TestTask
//
//  Created by Вячеслав Бельтюков on 17.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "ImageDownloadOperation.h"
#import "NetworkManager.h"
#import <UIKit/UIScreen.h>

@interface ImageDownloadOperation ()

@property (nonatomic) NSMutableSet *completionHandlers;
@property (nonatomic) NSString *url;

@end

@implementation ImageDownloadOperation

+ (instancetype)operationForURL:(NSString *)url withCompletionBlock:(DataBlock)completion{
    ImageDownloadOperation *operation = [self new];
    operation.completionHandlers = [NSMutableSet setWithObject:completion];
    operation.url = url;
    return operation;
}

- (void)main{
    DeclareWeakSelf
    NSString *path = [NSString stringWithFormat:@"photo/crop/%@_%.0fx%.0f.jpg", self.url, [UIScreen mainScreen].bounds.size.width, kCellHeight];
    [[NetworkManager sharedInstance] downloadDataFrom:path completionBlock:^(NSData *data) {
        [weakSelf completeWithData:data];
    }];
}

- (void)completeWithData:(NSData*)data{
    dispatch_async(dispatch_get_main_queue(), ^{
        for (DataBlock handler in self.completionHandlers) {
            handler(data);
        }
    });
}

- (void)addCompletionBlock:(DataBlock)block{
    [self.completionHandlers addObject:block];
}

@end
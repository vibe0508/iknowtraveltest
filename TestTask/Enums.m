//
//  Enums.m
//  TestTask
//
//  Created by Вячеслав Бельтюков on 17.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "Enums.h"

NSDictionary* ContentTypeMappings(){
    static NSDictionary *mappings = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        mappings = @{@(ContentTypeAddress): @"address",
                     @(ContentTypeArticle): @"article"};
    });
    return mappings;
}

NSDictionary* ContentTypeTitleMappings(){
    static NSDictionary *mappings = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        mappings = @{@(ContentTypeAddress): @"Места",
                     @(ContentTypeArticle): @"Заметки"};
    });
    return mappings;
}
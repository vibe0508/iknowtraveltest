//
//  ImageDownloadOperation.h
//  TestTask
//
//  Created by Вячеслав Бельтюков on 17.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageDownloadOperation : NSOperation

+ (instancetype)operationForURL:(NSString*)url withCompletionBlock:(DataBlock)completion;

- (void)addCompletionBlock:(DataBlock)block;

@end

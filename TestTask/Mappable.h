//
//  Mappable.h
//  TestTask
//
//  Created by Вячеслав Бельтюков on 17.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Mappable <NSObject>

+ (NSDictionary*)mappings;
+ (NSDictionary*)relationshipMappings;

+ (NSString*)dataKey;
+ (NSString*)pluralDataKey;

@property (nonatomic, nullable, retain) NSString *entityID;

@end
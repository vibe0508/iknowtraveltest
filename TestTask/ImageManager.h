//
//  ImageManager.h
//  TestTask
//
//  Created by Вячеслав Бельтюков on 17.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "Singleton.h"

@interface ImageManager : Singleton

- (void)getImageDataForURL:(NSString*)url toBlock:(DataBlock)block;

@end

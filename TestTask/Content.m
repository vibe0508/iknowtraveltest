//
//  Content.m
//  TestTask
//
//  Created by Вячеслав Бельтюков on 17.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "Content.h"
#import "Creator.h"

@implementation Content

+ (NSDictionary *)mappings{
    return @{@"entityID": @"_id",
             @"typeValue": @"type",
             @"title": @"title",
             @"rating": @"rating",
             @"cover": @"address_cover_1x.filename",
             @"self.cover": @"article_cover_1x.filename",
             @"creator": @"creator",
             @"categories": @"categories.name"
             };
}

+ (NSDictionary *)relationshipMappings{
    return @{@"creator": [Creator class]};
}

+ (NSString *)dataKey{
    return nil;
}

+ (NSString *)pluralDataKey{
    return nil;
}

- (ContentType)type{
    return self.typeValue.integerValue;
}

- (void)setType:(ContentType)type{
    self.typeValue = ContentTypeMappings()[@(type)];
}

@end

//
//  ContentManager.h
//  TestTask
//
//  Created by Вячеслав Бельтюков on 20.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "Singleton.h"
#import "Content.h"

@interface ContentManager : Singleton

- (void)getContentsOfType:(ContentType)type toBlock:(ArrayBlock)block;
- (void)loadMoreOfType:(ContentType)type sinceRating:(NSNumber*)rating toBlock:(ArrayBlock)block;
- (void)fetchInfoToContent:(Content*)content completion:(VoidBlock)completion;

@end

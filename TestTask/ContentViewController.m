//
//  ContentViewController.m
//  TestTask
//
//  Created by Вячеслав Бельтюков on 17.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "ContentViewController.h"

@interface ContentViewController ()

@property (nonatomic, weak) IBOutlet UILabel *creatorNameLabel;

@end

@implementation ContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self updateViews];
    DeclareWeakSelf
    self.viewModel.updateHandler = ^{
        [weakSelf updateViews];
    };
}

- (void)updateViews{
    self.creatorNameLabel.text = self.viewModel.creatorName;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

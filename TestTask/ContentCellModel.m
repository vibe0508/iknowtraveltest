//
//  ContentCellModel.m
//  TestTask
//
//  Created by Вячеслав Бельтюков on 17.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "ContentCellModel.h"
#import "ImageManager.h"

@implementation ContentCellModel

+ (instancetype)modelWithContent:(Content *)content{
    ContentCellModel *model = [self new];
    model.content = content;
    return model;
}

- (NSString *)title{
    return self.content.title;
}

- (NSString *)subtitle{
    NSMutableString *subtitle = [NSMutableString stringWithString:@"Категории:"];
    for (NSString *category in self.content.categories) {
        [subtitle appendFormat:@" %@,", category];
    }
    [subtitle deleteCharactersInRange:NSMakeRange(subtitle.length - 1, 1)];
    return subtitle;
}

- (void)getImageDataToBlock:(DataBlock)block{
    [[ImageManager sharedInstance] getImageDataForURL:self.content.cover toBlock:^(NSData *data) {
        if (block) {
            block(data);
        }
    }];
}

@end

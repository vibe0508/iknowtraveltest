//
//  ContentsListViewModel.h
//  TestTask
//
//  Created by Вячеслав Бельтюков on 17.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ContentViewModel;
@class ContentCellModel;

@interface ContentsListViewModel : NSObject

- (ContentCellModel*)cellModelAtIndex:(NSInteger)index;
- (ContentViewModel*)contentViewModelAtIndex:(NSInteger)index;
- (NSInteger)numberOfContents;
- (void)loadMore;

@property (nonatomic, readonly) NSArray* segmentTitles;
@property (nonatomic) NSInteger selectedSegment;
@property (nonatomic, copy) VoidBlock refreshBlock;
@property (nonatomic) BOOL displayNoContents;

@end

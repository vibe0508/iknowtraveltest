
//
//  ImageManager.m
//  TestTask
//
//  Created by Вячеслав Бельтюков on 17.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "ImageManager.h"
#import "Image.h"
#import "AppDelegate.h"
#import "NetworkManager.h"
#import "ImageDownloadOperation.h"

@interface ImageManager ()

@property (nonatomic) NSMutableDictionary *downloadOperations;

@end

@implementation ImageManager

- (instancetype)init{
    if (self = [super init]) {
        _downloadOperations = [NSMutableDictionary new];
    }
    return self;
}

- (void)getImageDataForURL:(NSString *)url toBlock:(DataBlock)block{
    if (!url) {
        return;
    }
    NSManagedObjectContext *context = MainContext;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Image"];
    request.predicate = [NSPredicate predicateWithFormat:@"url = %@", url];
    Image *image = [context executeFetchRequest:request error:nil].firstObject;
    if (image.data) {
        block(image.data);
        image.lastUsed = [NSDate date];
        return;
    }else{
        DeclareWeakSelf
        ImageDownloadOperation *operation = self.downloadOperations[url];
        if (!operation) {
            operation = [ImageDownloadOperation operationForURL:url withCompletionBlock:^(NSData *data) {
                block(data);
                [weakSelf saveData:data forURL:url];
            }];
            self.downloadOperations[url] = operation;
            [operation start];
        }else{
            [operation addCompletionBlock:block];
        }
    }
}

- (void)saveData:(NSData*)data forURL:(NSString*)url{
    dispatch_async(dispatch_get_main_queue(), ^{
        Image *image = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Image class]) inManagedObjectContext:MainContext];
        image.data = data;
        image.url = url;
        image.lastUsed = [NSDate date];
        SaveMainContext
    });
}

@end
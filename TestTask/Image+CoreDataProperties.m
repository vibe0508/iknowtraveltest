//
//  Image+CoreDataProperties.m
//  TestTask
//
//  Created by Вячеслав Бельтюков on 17.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Image+CoreDataProperties.h"

@implementation Image (CoreDataProperties)

@dynamic url;
@dynamic data;
@dynamic lastUsed;

@end

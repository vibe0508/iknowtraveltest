//
//  Content+CoreDataProperties.h
//  TestTask
//
//  Created by Вячеслав Бельтюков on 17.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Content.h"
@class Creator;

NS_ASSUME_NONNULL_BEGIN

@interface Content (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSString *cover;
@property (nullable, nonatomic, retain) NSString *entityID;
@property (nullable, nonatomic, retain) NSString *typeValue;
@property (nullable, nonatomic, retain) NSNumber *rating;
@property (nullable, nonatomic, retain) NSArray *categories;

@property (nullable, nonatomic, retain) Creator *creator;

@end

NS_ASSUME_NONNULL_END

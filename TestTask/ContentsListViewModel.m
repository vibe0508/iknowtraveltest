//
//  ContentsListViewModel.m
//  TestTask
//
//  Created by Вячеслав Бельтюков on 17.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "ContentsListViewModel.h"
#import "ContentManager.h"

#import "ContentCellModel.h"
#import "ContentViewModel.h"

@interface ContentsListViewModel ()

@property (nonatomic) NSArray *itemModels;
@property (nonatomic) BOOL loadMoreExecuting;
@property (nonatomic) ContentType contentType;

@end

@implementation ContentsListViewModel

- (instancetype)init{
    if (self = [super init]) {
        self.selectedSegment = 0;
    }
    return self;
}

- (void)makeModelsFromContents:(NSArray*)contents{
    NSMutableArray *models = [NSMutableArray new];
    for (Content *content in contents) {
        [models addObject:[ContentCellModel modelWithContent:content]];
    }
    self.itemModels = models;
}

- (NSArray *)segmentTitles{
    return @[@"Все", @"Заметки", @"Места"];
}

- (void)setSelectedSegment:(NSInteger)selectedSegment{
    _selectedSegment = selectedSegment;
    self.contentType = selectedSegment == 0 ? ContentTypeAddress|ContentTypeArticle : selectedSegment == 1 ? ContentTypeArticle:ContentTypeAddress;
    self.displayNoContents = NO;
    DeclareWeakSelf
    [[ContentManager sharedInstance] getContentsOfType:self.contentType toBlock:^(NSArray *array) {
        weakSelf.itemModels = [weakSelf modelsWithContents:array];
        [weakSelf refreshView];
    }];
}

- (void)setItemModels:(NSArray *)itemModels{
    _itemModels = itemModels;
    [self refreshView];
}

- (ContentCellModel *)cellModelAtIndex:(NSInteger)index{
    if (index + 2 < self.itemModels.count) {
        [self.itemModels[index+2] getImageDataToBlock:^(NSData *data) {}];
    }
    return self.itemModels[index];
}

- (NSInteger)numberOfContents{
    return self.itemModels.count;
}

- (NSArray*)modelsWithContents:(NSArray*)contents{
    NSMutableArray *models = [NSMutableArray array];
    for (Content *cont in contents) {
        [models addObject:[ContentCellModel modelWithContent:cont]];
    }
    return models;
}

- (void)loadMore{
    if (self.loadMoreExecuting) {
        return;
    }
    self.loadMoreExecuting = YES;
    ContentType typeCheck = self.contentType;
    DeclareWeakSelf
    [[ContentManager sharedInstance] loadMoreOfType:self.contentType sinceRating:[self.itemModels.lastObject content].rating toBlock:^(NSArray *array) {
        weakSelf.loadMoreExecuting = NO;
        if (weakSelf.contentType != typeCheck) {
            return;
        }
        if (array.count > 0) {
            weakSelf.itemModels = [weakSelf.itemModels arrayByAddingObjectsFromArray:[weakSelf modelsWithContents:array]];
        }else{
            weakSelf.displayNoContents = YES;
        }
        [weakSelf refreshView];
    }];
}

- (void)refreshView{
    DeclareWeakSelf
    dispatch_async(dispatch_get_main_queue(), ^{
        if (weakSelf.refreshBlock) {
            weakSelf.refreshBlock();
        }
    });
}

- (ContentViewModel *)contentViewModelAtIndex:(NSInteger)index{
    return [ContentViewModel viewModelForContent:[self.itemModels[index] content]];
}

@end

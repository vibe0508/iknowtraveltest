//
//  Creator.m
//  TestTask
//
//  Created by Вячеслав Бельтюков on 20.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "Creator.h"

@implementation Creator

+ (NSDictionary *)relationshipMappings{
    return nil;
}

+ (NSDictionary *)mappings{
    return @{@"entityID": @"_id",
             @"name": @"name"};
}

@end

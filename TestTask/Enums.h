//
//  Enums.h
//  TestTask
//
//  Created by Вячеслав Бельтюков on 17.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    ContentTypeArticle = 1,
    ContentTypeAddress = 1 << 1
} ContentType;

extern NSDictionary* ContentTypeMappings();
extern NSDictionary* ContentTypeTitleMappings();
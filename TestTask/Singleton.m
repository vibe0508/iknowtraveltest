//
//  Singleton.m
//  TestTask
//
//  Created by Вячеслав Бельтюков on 17.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "Singleton.h"

@implementation Singleton

+ (instancetype)sharedInstance{
    static NSMutableDictionary *instances = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instances = [NSMutableDictionary dictionary];
    });
    Singleton *instance = instances[NSStringFromClass(self)];
    if (!instance) {
        instance = [self new];
        instances[NSStringFromClass(self)] = instance;
    }
    return instance;
}

@end

//
//  Creator+CoreDataProperties.h
//  TestTask
//
//  Created by Вячеслав Бельтюков on 20.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Creator.h"

NS_ASSUME_NONNULL_BEGIN

@interface Creator (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *entityID;
@property (nullable, nonatomic, retain) NSString *name;

@end

NS_ASSUME_NONNULL_END

//
//  NetworkManager.h
//  TestTask
//
//  Created by Вячеслав Бельтюков on 17.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "Singleton.h"

@interface NetworkManager : Singleton

- (void) runRquestTo:(NSString*)path withMethod:(NSString*)method params:(NSDictionary*)params completionBlock:(ObjectBlock)completionBlock errorBlock:(ErrorBlock)errorBlock;
- (void) downloadDataFrom:(NSString*)path completionBlock:(DataBlock)completionBlock;



@end

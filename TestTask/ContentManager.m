//
//  ContentManager.m
//  TestTask
//
//  Created by Вячеслав Бельтюков on 20.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "ContentManager.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "NetworkManager.h"
#import "Mapper.h"

NSString * const kContentsPath = @"contents";
NSString * const kSingleContentPath = @"content";
NSString * const kGETmethod = @"GET";

@implementation ContentManager

- (void)getContentsOfType:(ContentType)type toBlock:(ArrayBlock)block{
    NSFetchRequest *req = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Content class])];
    req.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"rating" ascending:NO]];
    if (type != (ContentTypeAddress | ContentTypeArticle)) {
        req.predicate = [NSPredicate predicateWithFormat:@"typeValue = %@", ContentTypeMappings()[@(type)]];
    }
    block([MainContext executeFetchRequest:req error:nil]);
    [self makeRequestForType:type since:nil withBlock:block];
}

- (void)loadMoreOfType:(ContentType)type sinceRating:(NSNumber *)rating toBlock:(ArrayBlock)block{
    [self makeRequestForType:type since:rating withBlock:block];
}

- (void)makeRequestForType:(ContentType)type since:(NSNumber*)rating withBlock:(ArrayBlock)block{
    NSMutableDictionary *params = [NSMutableDictionary new];
    if (rating) {
        params[@"since"] = rating.stringValue;
    }
    if (type != (ContentTypeAddress | ContentTypeArticle)) {
        params[@"type"] = ContentTypeMappings()[@(type)];
    }
    params[@"limit"] = @30;
    [[NetworkManager sharedInstance] runRquestTo:kContentsPath withMethod:kGETmethod params:params completionBlock:^(id obj) {
        block([[[Mapper sharedInstance] mapObjectsFromArray:obj withClass:[Content class]] sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"rating" ascending:NO]]]);
        dispatch_async(dispatch_get_main_queue(), ^{
            SaveMainContext
        });
    } errorBlock:^(NSError *error) {
        NSLog(@"Error while downloading contents: %@", error);
    }];
}

- (void)fetchInfoToContent:(Content *)content completion:(VoidBlock)completion{
    [[NetworkManager sharedInstance] runRquestTo:[kSingleContentPath stringByAppendingPathComponent:content.entityID] withMethod:kGETmethod params:nil completionBlock:^(id obj) {
        [[Mapper sharedInstance] mapObject:obj withClass:[Content class]];
        dispatch_async(dispatch_get_main_queue(), ^{
            SaveMainContext
            completion();
        });
    } errorBlock:^(NSError *error) {
        NSLog(@"Error while downloading contents: %@", error);
    }];
}

@end
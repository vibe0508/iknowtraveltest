//
//  NetworkManager.m
//  TestTask
//
//  Created by Вячеслав Бельтюков on 17.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "NetworkManager.h"

NSString * const baseURL = @"http://iknow.travel/";
NSString * const dataURL = @"http://cdn1.iknow.travel/";

@interface NetworkManager()

@property (nonatomic) NSURLSession *session;

@end

@implementation NetworkManager

- (instancetype)init{
    if (self = [super init]) {
        _session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    }
    return self;
}

- (void)runRquestTo:(NSString *)path withMethod:(NSString *)method params:(NSDictionary*)params completionBlock:(ObjectBlock)completionBlock errorBlock:(ErrorBlock)errorBlock{
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:[self requestToPath:path withParams:params method:method] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error || !data) {
            errorBlock(error);
            return;
        }
        NSError *jsonError;
        id result = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
        if (jsonError || !result) {
            errorBlock(error);
        }else{
            completionBlock(result);
        }
    }];
    [task resume];
}

- (void)downloadDataFrom:(NSString *)path completionBlock:(DataBlock)completionBlock{
    NSURLSessionDownloadTask *task = [self.session downloadTaskWithURL:[NSURL URLWithString:[dataURL stringByAppendingString:path]]
                                                     completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                                         if (!error) {
                                                             completionBlock([NSData dataWithContentsOfURL:location]);
                                                         }
                                                     }];
    task.resume;
}

- (NSURLRequest*)requestToPath:(NSString*)path withParams:(NSDictionary*)params method:(NSString*)method{
    NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@%@",baseURL, path];
    if ([method isEqualToString:@"GET"] && params) {
        [urlString appendString:@"?"];
        for (NSString *key in params.allKeys) {
            [urlString appendFormat:@"%@=%@&",key, params[key]];
        }
        [urlString deleteCharactersInRange:NSMakeRange(urlString.length - 1, 1)];
    }
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    request.HTTPMethod = method;
    [request setValue:@"2" forHTTPHeaderField:@"X-Api-Version"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    return request;
}

@end

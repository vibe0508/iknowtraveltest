//
//  ContentsListViewController.m
//  TestTask
//
//  Created by Вячеслав Бельтюков on 17.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "ContentsListViewController.h"
#import "ContentTableViewCell.h"
#import "ContentViewController.h"

@interface ContentsListViewController ()

@property (nonatomic, weak) IBOutlet UISegmentedControl *typeSelector;
@property (nonatomic, strong) IBOutlet UIView *noContentsView;

@end

@implementation ContentsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    DeclareWeakSelf
    self.viewModel = [ContentsListViewModel new];
    self.viewModel.refreshBlock = ^{
        [weakSelf.tableView reloadData];
        weakSelf.tableView.tableFooterView = weakSelf.viewModel.displayNoContents ? weakSelf.noContentsView : nil;
    };
    
    for (int i =0; i< self.viewModel.segmentTitles.count; i++) {
        [self.typeSelector setTitle:self.viewModel.segmentTitles[i] forSegmentAtIndex:i];
    }
    self.typeSelector.selectedSegmentIndex = self.viewModel.selectedSegment;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)changedSelectedSegment:(UISegmentedControl*)sender{
    self.viewModel.selectedSegment = sender.selectedSegmentIndex;
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.viewModel.numberOfContents;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ContentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.model = [self.viewModel cellModelAtIndex:indexPath.row];
    
    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentSize.height - scrollView.contentOffset.y - scrollView.frame.size.height < 100) {
        [self.viewModel loadMore];
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    ((ContentViewController*)segue.destinationViewController).viewModel = [self.viewModel contentViewModelAtIndex:[self.tableView indexPathForCell:sender].row];
}

@end

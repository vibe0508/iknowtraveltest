//
//  Mapper.m
//  TestTask
//
//  Created by Вячеслав Бельтюков on 20.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "Mapper.h"
#import "Mappable.h"
#import "AppDelegate.h"
#import <CoreData/CoreData.h>

@interface Mapper ()

@property (nonatomic) NSMutableDictionary *classesForSingleObjects;
@property (nonatomic) NSMutableDictionary *classesForPluralObjects;
@property (nonatomic) NSManagedObjectContext *context;

@end

@implementation Mapper

- (instancetype)init{
    if (self = [super init]) {
        _classesForPluralObjects = [NSMutableDictionary new];
        _classesForSingleObjects = [NSMutableDictionary new];
    }
    return self;
}

- (void)registerClass:(Class)aClass{
    if (![aClass conformsToProtocol:@protocol(Mappable)]) {
        return;
    }
    if ([aClass dataKey]) {
        self.classesForSingleObjects[[aClass dataKey]] = aClass;
    }
    if ([aClass pluralDataKey]) {
        self.classesForPluralObjects[[aClass pluralDataKey]] = aClass;
    }
}

- (NSArray*)mapFromDictionary:(NSDictionary *)dict{
    NSMutableArray *results = [NSMutableArray new];
    for (NSString *key in dict.allKeys) {
        id obj = dict[key];
        if ([obj isKindOfClass:[NSArray class]] && self.classesForPluralObjects[key]) {
            [results addObjectsFromArray:[self mapObjectsFromArray:obj withClass:self.classesForPluralObjects[key]]];
        }
        if ([obj isKindOfClass:[NSDictionary class]] && self.classesForSingleObjects[key]) {
            [results addObjectsFromArray:[self mapObjectsFromArray:obj withClass:self.classesForSingleObjects[key]]];
        }
    }
    return results;
}

- (NSArray*)mapObjectsFromArray:(NSArray*)array withClass:(Class)aClass{
    NSMutableArray *results = [NSMutableArray new];
    for (id obj in array) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            [results addObject:[self mapObject:obj withClass:aClass]];
        }
    }
    return results;
}

- (id)mapObject:(NSDictionary*)dict withClass:(Class)aClass{
    NSManagedObject<Mappable> *entity = [self entityWithID:dict[[aClass mappings][@"entityID"]] ofClass:aClass];
    NSDictionary *mappings = [aClass mappings];
    NSDictionary *relationshipMappings = [aClass relationshipMappings];
    for (NSString *entityKey in mappings.allKeys) {
        if (![dict valueForKeyPath:mappings[entityKey]]){
            continue;
        }else if (relationshipMappings[entityKey]) {
            [entity setValue:[self mapObject:[dict valueForKeyPath:mappings[entityKey]] withClass:relationshipMappings[entityKey]] forKeyPath:entityKey];
        }else{
            [entity setValue:[dict valueForKeyPath:mappings[entityKey]] forKeyPath:entityKey];
        }
    }
    return entity;
}

- (NSManagedObject<Mappable>*)entityWithID:(NSString*)entityID ofClass:(Class)aClass{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass(aClass)];
    request.predicate = [NSPredicate predicateWithFormat:@"entityID = %@", entityID];
    NSManagedObject<Mappable> *object = [MainContext executeFetchRequest:request error:nil].firstObject;
    if (object) {
        return object;
    }
    object = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass(aClass) inManagedObjectContext:MainContext];
    object.entityID = entityID;
    return object;
}

@end

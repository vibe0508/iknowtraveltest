//
//  ContentViewController.h
//  TestTask
//
//  Created by Вячеслав Бельтюков on 17.07.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContentViewModel.h"

@interface ContentViewController : UIViewController

@property (nonatomic) ContentViewModel *viewModel;

@end
